# Solid Bucket

### A universal API for Cloud Storage providers
Use the same API to communicate with every Cloud providers.  We built a solid abstraction that you can rely on.

### Supported Providers:
* Amazon AWS S3
* Backblaze B2
* Google Cloud Storage
* Microsoft Azure Blob Storage
* DigitalOcean Spaces
* Rackspace Cloud Storage
* Wasabi Object Storage
* Any S3 Compatible Storage (e.g Minio)
* Local Folder (e.g Dropbox, NAS...)

## Changelog

#### v. 2.0.0
This version introduces a completely new API, so it contains code-breaking changes. We encorauge you to update, but please keep in mind that some refactoring will be required.

## 1. How to install it

```javascript
npm install solid-bucket
```

## 2. How to Authenticate

### 2.1 Amazon AWS S3
```javascript
const SolidBucket = require('solid-bucket')

let provider = new SolidBucket('aws', {
    accessKeyId: 'accessKeyId',
    secretAccessKey: 'secretAccessKey',
    region: 'us-east-1' // Optional: "us-east-1" by default
})
```

### 2.2 Backblaze B2
```javascript
const SolidBucket = require('solid-bucket')

let provider = new SolidBucket('b2', {
    accountId: 'accountId',
    applicationKey: 'applicationKey'
})
```

### 2.3 Google Cloud Storage
```javascript
const SolidBucket = require('solid-bucket')

// How to generate a credentials JSON file: https://cloud.google.com/storage/docs/authentication (To generate a private key in JSON format)
let provider = new SolidBucket('gcs', {
    keyFilename: 'keyFilename'
})
```

### 2.4 Microsoft Azure Blob Storage
```javascript
const SolidBucket = require('solid-bucket')

let provider = new SolidBucket('azure', {
    accountName: 'accountName',
    accountKey: 'accountKey'
})
```

### 2.5 DigitalOcean Spaces
```javascript
const SolidBucket = require('solid-bucket')

let provider = new SolidBucket('digitalocean', {
    accessKeyId: 'accessKeyId',
    secretAccessKey: 'secretAccessKey',
    region: 'nyc3' // Optional: "nyc3" by default
})
```

### 2.6 Rackspace Cloud Storage
```javascript
const SolidBucket = require('solid-bucket')

let provider = new SolidBucket('rackspace', {
    username: 'username',
    apiKey: 'apiKey',
    region: 'IAD' // Optional: 'IAD' by default
})
```

### 2.7 Wasabi Object Storage
```javascript
const SolidBucket = require('solid-bucket')

let provider = new SolidBucket('wasabi', {
    accessKeyId: 'accessKeyId',
    secretAccessKey: 'secretAccessKey',
})
```

### 2.8 Any S3 Compatible Storage (e.g Minio)
```javascript
const SolidBucket = require('solid-bucket')

let provider = new SolidBucket('s3compatible', {
    endpoint: 'endpoint',
    accessKeyId: 'accessKeyId',
    secretAccessKey: 'secretAccessKey',
})
```

### 2.9 Local Folder (e.g Dropbox, NAS...)
```javascript
const SolidBucket = require('solid-bucket')

let provider = new SolidBucket('folder', {
    folderPath: 'folderPath'
})
```

## 3. Universal API

### 3.1 Create a Bucket

#### 3.1.1 Definition
```javascript
provider.createBucket(bucketName) // returns a Promise
```
#### 3.1.2 Full Example
```javascript
const SolidBucket = require('solid-bucket')

let provider = new SolidBucket('azure', {
    accountName: 'accountName',
    accountKey: 'accountKey'
})

let bucketName = 'example'
provider.createBucket(bucketName).then((resp) => {
    if (resp.status === 201) {
        console.log(resp.message) 
        // Output: Bucket "example" was created successfully
    }
}).catch((resp) => {
    if (resp.status === 400){
        console.log(resp.message)
        // Output: Some error coming from the provider...
    }
})
```
### 3.2 Delete a Bucket

#### 3.2.1 Definition
```javascript
provider.deleteBucket(bucketName) //  returns a Promise
```

#### 3.2.2 Full Example
```javascript
const SolidBucket = require('solid-bucket')

let provider = new SolidBucket('azure', {
    accountName: 'accountName',
    accountKey: 'accountKey'
})

let bucketName = 'example'
provider.deleteBucket(bucketName).then((resp) => {
    if (resp.status === 200) {
        console.log(resp.message) 
        // Output: Bucket "example" was deleted successfully
    }
}).catch((resp) => {
    if (resp.status === 400){
        console.log(resp.message) 
        // Output: Some error coming from the provider...
    }
})
```

### 3.3 Upload File

#### 3.3.1 Definition
```javascript
provider.uploadFile(bucketName, filePath) //  returns a Promise
```

#### 3.3.2 Full Example
```javascript
const SolidBucket = require('solid-bucket')

let provider = new SolidBucket('azure', {
    accountName: 'accountName',
    accountKey: 'accountKey'
})

let bucketName = 'example'
let filePath = '/tmp/file.bin'
provider.uploadFile(bucketName, filePath).then((resp) => {
    if (resp.status === 200) {
        console.log(resp.message) 
        // Output: Bucket "example" was deleted successfully
    }
}).catch((resp) => {
    if (resp.status === 400){
        console.log(resp.message) 
        // Output: Some error coming from the provider...
    }
})
```

### 3.4 Download File

#### 3.4.1 Definition
```javascript
provider.deleteBucket(bucketName, remoteFilename downloadedFilePath) //  returns a Promise
```

#### 3.4.2 Full Example
```javascript
const SolidBucket = require('solid-bucket')

let provider = new SolidBucket('azure', {
    accountName: 'accountName',
    accountKey: 'accountKey'
})

let bucketName = 'example'
let remoteFilename = 'file.bin'
let downloadedFilePath = '/tmp'
provider.downloadFile(bucketName, remoteFilename, downloadedFilePath).then((resp) => {
    if (resp.status === 200) {
        console.log(resp.message) 
        // Output: Bucket "example" was deleted successfully
    }
}).catch((resp) => {
    if (resp.status === 400){
        console.log(resp.message) 
        // Output: Some error coming from the provider...
    }
})
```

### 3.5 Create File from Text in Bucket

#### 3.5.1 Definition
```javascript
provider.createFileFromText(bucketName, remoteFilename, text) // returns a Promise
```

#### 3.5.2 Full Example
```javascript
const SolidBucket = require('solid-bucket')

let provider = new SolidBucket('azure', {
    accountName: 'accountName',
    accountKey: 'accountKey'
})

let bucketName = 'example'
let remoteFilename = 'my_remote_object.txt'
let text = 'This is a text that I would like to upload'
provider.createFileFromText(bucketName, remoteFilename, text).then((resp) => {
    if (resp.status === 200) {
        console.log(resp.message) 
        // Output: Object "example.txt" was saved successfully in bucket "example"
    }
}).catch((resp) => {
    if (resp.status === 400){
        console.log(resp.message)
        // Output: Some error coming from the provider...
    }
})
```

### 3.6 Delete file from Bucket

#### 3.6.1 Definition
```javascript
provider.deleteFile(bucketName, remoteFilename) // returns a Promise
```

#### 3.6.2 Full Example
```javascript
const SolidBucket = require('solid-bucket')

let provider = new SolidBucket('azure', {
    accountName: 'accountName',
    accountKey: 'accountKey'
})

let bucketName = 'example'
let remoteFilename = 'my_remote_object.txt'

provider.deleteFile(bucketName, remoteFilename).then((resp) => {
    if (resp.status === 200) {
        console.log(resp.message) 
        // Output: Object "example.txt" was deleted successfully from bucket "example"
    }
}).catch((resp) => {
    if (resp.status === 400){
        console.log(resp.message)
        // Output: Some error coming from the provider...
    }
})
```

### 3.7 Read File from Bucket

#### 3.7.1 Definition
```javascript
provider.readFile(bucketName, remoteFilename) // returns a Promise
```

#### 3.7.2 Full Example
```javascript
const SolidBucket = require('solid-bucket')

let provider = new SolidBucket('azure', {
    accountName: 'accountName',
    accountKey: 'accountKey'
})

let bucketName = 'example'
let remoteFilename = 'my_remote_object.txt'
provider.readFile(bucketName, remoteFilename).then((resp) => {
    if (resp.status === 200) {
        console.log(resp.message) 
        // Output: Object "example.txt" was fetched successfully from bucket "example"
    }
}).catch((resp) => {
    if (resp.status === 400){
        console.log(resp.message)
        // Output: Some error coming from the provider...
    }
})
```

### 3.8 Get list of objects from Bucket

#### 3.8.1 Definition
```javascript
provider.getListOfFiles(bucketName) // returns a Promise
```

#### 3.8.2 Full Example
```javascript
const SolidBucket = require('solid-bucket')

let provider = new SolidBucket('azure', {
    accountName: 'accountName',
    accountKey: 'accountKey'
})

let bucketName = 'example'
provider.getListOfFiles(bucketName).then((resp) => {
    if (resp.status === 200) {
        console.log(resp.message) 
        // Output: The list of objects was fetched successfully from bucket "example"
    }
}).catch((resp) => {
    if (resp.status === 400){
        console.log(resp.message)
        // Output: Some error coming from the provider...
    }
})
```
## 4. Tests

Required Environmental vars depending on the provider:
#### Amazon AWS S3

* AWS_ACCESSKEYID
* AWS_SECRETACCESSKEY

#### Backblaze B2

* B2_ACCOUNTID
* B2_APPLICATIONKEY

#### DigitalOcean Spaces

* DIGITALOCEAN_ACCESSKEYID
* DIGITALOCEAN_SECRETACCESSKEY

#### Google Cloud Storage

* GCS_PROJECTID
* GCS_PRIVATEKEYID
* GCS_PRIVATEKEY
* GCS_CLIENTEMAIL
* GCS_CLIENTID
* GCS_CLIENTX509CERTURL

#### Microsoft Azure

* AZURE_ACCOUNTNAME
* AZURE_ACCOUNTKEY

#### Rackspace Cloud Storage

* RACKSPACE_USERNAME
* RACKSPACE_APIKEY

#### Wasabi Object Storage

* WASABI_ACCESSKEYID
* WASABI_SECRETACCESSKEY

#### Any S3 Compatible Storage
* S3COMPATIBLE_ENDPOINT
* S3COMPATIBLE_ACCESSKEYID
* S3COMPATIBLE_SECRETACCESSKEY

#### Folder

* FOLDER_FOLDERPATH

Afterwards, just run:
```javascript
npm run compile && npm test
```

## 5. License
MIT