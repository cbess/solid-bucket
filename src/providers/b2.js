const axios = require('axios');
const crypto = require('crypto');
const { Readable, Writable } = require('stream');
const crc32 = require('fast-crc32c');
const path = require('path');
const fs = require('fs');


function getB2(options) {
	return new Promise((resolve, reject) => {
		let encodedBase64 = new Buffer(`${options.accountId}:${options.applicationKey}`).toString('base64')
		axios.post(
			`https://api.backblazeb2.com/b2api/v1/b2_authorize_account`,
			{},
			{
				headers: { Authorization: `Basic ${encodedBase64}` }
			})
			.then(function (response) {
				let data = response.data
				let credentials = {
					accountId: options.accountId,
					applicationKey: options.applicationKey,
					apiUrl: data.apiUrl,
					authorizationToken: data.authorizationToken,
					downloadUrl: data.downloadUrl,
					recommendedPartSize: data.recommendedPartSize
				}
				resolve(new B2Wrapper(credentials))
			})
			.catch(function (err) {
				throw new Error({
					status: 400,
					message: err.message
				})
			});
	})
}

class B2Wrapper {
	constructor(credentials) {
		this.credentials = credentials
		let self = this

		let getBucketByName = (bucketName) => {
			return new Promise((resolve, reject) => {
				axios.post(
					`${self.credentials.apiUrl}/b2api/v1/b2_list_buckets`,
					{
						accountId: self.credentials.accountId,
						bucketTypes: ["allPrivate", "allPublic"]
					},
					{ headers: { Authorization: self.credentials.authorizationToken } })
					.then(function (response) {
						let buckets = response.data.buckets
						let bucket = null
						buckets.forEach((element) => {
							if (element.bucketName === bucketName) {
								bucket = element
							}
						})

						if (!bucket) {
							reject({
								status: 400,
								message: 'Bucket not found'
							})
						} else {
							resolve({
								status: 200,
								data: bucket
							})
						}
					})
					.catch(function (err) {
						reject({
							status: 400,
							message: err.message
						})
					});
			})
		}

		let getFileByName = (bucketName, filename) => {
			return new Promise((resolve, reject) => {
				getBucketByName(bucketName).then((response) => {
					if (response.status !== 200) {
						reject({
							status: 400,
							message: response.message
						})
					}
					let bucket = response.data
					axios.post(
						`${self.credentials.apiUrl}/b2api/v1/b2_list_file_names`,
						{
							bucketId: bucket.bucketId
						},
						{ headers: { Authorization: self.credentials.authorizationToken } })
						.then(function (response) {
							let files = response.data.files
							let file = null
							files.forEach((element) => {
								if (element.fileName === filename) {
									file = element
								}
							})
							if (!file) {
								reject({
									status: 400,
									message: 'File not found'
								})
							} else {
								resolve({
									status: 200,
									data: file
								})
							}
						})
						.catch(function (err) {
							reject({
								status: 400,
								message: err.message
							})
						});
				}).catch(function (err) {
					reject({
						status: 400,
						message: err.message
					})
				});
			});
		}

		let getUploadUrl = (bucketId) => {
			return new Promise((resolve, reject) => {
				axios.post(
					`${self.credentials.apiUrl}/b2api/v1/b2_get_upload_url`,
					{
						bucketId: bucketId
					},
					{ headers: { Authorization: self.credentials.authorizationToken } })
					.then(function (response) {
						resolve({
							status: 200,
							data: response.data,
							message: `UploadURL was received successfully`
						})
					})
					.catch(function (err) {
						reject({
							status: 400,
							message: err.message
						})
					});
			})
		}

		let getUploadPartUrl = (fileId) => {
			return new Promise((resolve, reject) => {
				axios.post(
					`${self.credentials.apiUrl}/b2api/v1/b2_get_upload_part_url`,
					{
						fileId: fileId
					},
					{ headers: { Authorization: self.credentials.authorizationToken } })
					.then(function (response) {
						resolve({
							status: 200,
							data: response.data,
							message: `UploadPartURL was received successfully`
						})
					})
					.catch(function (err) {
						reject({
							status: 400,
							message: err.message
						})
					});
			})
		}

		let startLargeFileUpload = (filename, bucketId) => {
			return new Promise((resolve, reject) => {
				let data = {
					fileName: filename,
					bucketId: bucketId,
					contentType: 'b2/x-auto'
				}

				axios.post(
					`${self.credentials.apiUrl}/b2api/v1/b2_start_large_file`,
					data,
					{ headers: { Authorization: self.credentials.authorizationToken } })
					.then(function (response) {
						resolve({
							status: 200,
							data: response.data,
							message: `Upload for large File "${filename}" was successfully started`
						})
					})
					.catch(function (err) {
						reject({
							status: 400,
							message: err.message
						})
					});
			})
		}

		let finishLargeFileUpload = (fileId, filePath, bucketName, listOfSha1) => {
			return new Promise((resolve, reject) => {
				let data = {
					fileId: fileId,
					partSha1Array: listOfSha1
				}
				axios.post(
					`${self.credentials.apiUrl}/b2api/v1/b2_finish_large_file`,
					data,
					{ headers: { Authorization: self.credentials.authorizationToken } })
					.then(function (response) {
						resolve({
							status: 200,
							message: `File "${filePath}" was uploaded successfully to bucket "${bucketName}"`
						})
					})
					.catch(function (err) {
						reject({
							status: 400,
							message: err.message
						})
					});
			})
		}

		let cancelLargeFileUpload = (fileId, filePath) => {
			return new Promise((resolve, reject) => {
				let data = {
					fileId: fileId
				}
				axios.post(
					`${self.credentials.apiUrl}/b2api/v1/b2_cancel_large_file`,
					data,
					{ headers: { Authorization: self.credentials.authorizationToken } })
					.then(function (response) {
						resolve({
							status: 200,
							message: `Large File "${filePath}" upload was unsuccessful`
						})
					})
					.catch(function (err) {
						reject({
							status: 400,
							message: err.message
						})
					});
			})
		}

		let uploadSmallFile = (source, fileName, filePath, fileSize, sha1, uploadUrl, uploadAuthorizationToken, bucketName) => {
			return new Promise((resolve, reject) => {
				return axios.post(
					uploadUrl,
					source,
					{
						headers: {
							Authorization: uploadAuthorizationToken,
							"X-Bz-File-Name": fileName,
							"Content-Type": "b2/x-auto",
							"Content-Length": fileSize,
							"X-Bz-Content-Sha1": sha1,
							"X-Bz-Info-Author": "unknown"
						}
					}
				).then(function (response) {
					resolve({
						status: 200,
						message: `File "${filePath}" was uploaded successfully to bucket "${bucketName}"`
					})
				}).catch(function (err) {
					reject({
						status: 400,
						message: err.message
					})
				});
			})
		}

		let uploadBigFile = (source, fileName, filePath, fileSize, eachPartSize, bucketId, bucketName, shouldSimulateBigFileUploadError = false) => {
			return new Promise((resolve, reject) => {

				startLargeFileUpload(fileName, bucketId).then((response) => {
					let fileId = response.data.fileId;

					getUploadPartUrl(fileId).then((response) => {
						let fileId = response.data.fileId;
						let uploadUrl = response.data.uploadUrl;
						let authorizationToken = response.data.authorizationToken;
						let numOfParts = (eachPartSize > fileSize) ? 1 : Math.ceil(fileSize / eachPartSize);
						let lastPartSize = (eachPartSize > fileSize) ? fileSize : fileSize - (Math.floor(fileSize / eachPartSize) * eachPartSize);
						let partCounter = 0;
						let listOfSha1 = [];
						const destination = new Writable({
							write(chunk, encoding, callback) {
								partCounter += 1;

								let hash = crypto.createHash('sha1');

								hash.setEncoding('hex');
								hash.write(chunk);
								hash.end();
								let sha1 = hash.read();
								listOfSha1.push(sha1);

								axios.post(
									uploadUrl,
									chunk,
									{
										headers: {
											Authorization: authorizationToken,
											"X-Bz-Part-Number": partCounter,
											"Content-Length": (partCounter !== numOfParts) ? eachPartSize : lastPartSize,
											"X-Bz-Content-Sha1": sha1
										}
									}
								).then((response) => {
									callback();
								}).catch((err) => {
									cancelLargeFileUpload(fileId, filePath).then((response) => {
										reject(err)
									}).catch((err) => {
										reject({
											status: 400,
											message: `Large File "${filePath}" upload was unsuccessful`
										})
									})
								})
							}
						});

						source.on('data', (data) => {
							if (partCounter === 2 && shouldSimulateBigFileUploadError) {
								cancelLargeFileUpload(fileId, filePath).then((response) => {
									reject({
										status: 400,
										message: `Large File "${filePath}" upload was unsuccessful`
									})
								})
							}
						})

						source.on('error', (err) => {
							cancelLargeFileUpload(fileId, filePath).then((response) => {
								reject(err)
							})
						})

						destination.on('error', (err) => {
							cancelLargeFileUpload(fileId, filePath).then((response) => {
								reject(err)
							})
						})

						destination.on('finish', (value) => {
							finishLargeFileUpload(fileId, filePath, bucketName, listOfSha1).then((response) => {
								resolve(response)
							}).catch((err) => {
								cancelLargeFileUpload(fileId, filePath).then((response) => {
									reject(err)
								})
							})
						})
						source.pipe(destination)
					})
				})
			})
		}

		self.createBucket = (name) => {
			if (!name) {
				return new Promise((resolve, reject) => {
					reject({
						status: 400,
						message: 'Invalid parameters'
					})
				})
			}

			return new Promise((resolve, reject) => {
				axios.get(
					`${self.credentials.apiUrl}/b2api/v1/b2_create_bucket?accountId=${self.credentials.accountId}&bucketName=${name}&bucketType=allPrivate`,
					{ headers: { Authorization: self.credentials.authorizationToken } })
					.then(function (response) {
						resolve({
							status: 201,
							message: `Bucket "${name}" was created successfully`
						})
					})
					.catch(function (err) {
						reject({
							status: 400,
							message: err.message
						})
					});
			})
		}

		self.deleteBucket = (name) => {
			if (!name) {
				return new Promise((resolve, reject) => {
					reject({
						status: 400,
						message: 'Invalid parameters'
					})
				})
			}

			return new Promise((resolve, reject) => {
				getBucketByName(name).then((response) => {
					if (response.status !== 200) {
						reject({
							status: 400,
							message: response.message
						})
					}

					let bucket = response.data

					axios.post(
						`${self.credentials.apiUrl}/b2api/v1/b2_delete_bucket`,
						{
							accountId: self.credentials.accountId,
							bucketId: bucket.bucketId
						},
						{ headers: { Authorization: self.credentials.authorizationToken } })
						.then(function (response) {
							resolve({
								status: 200,
								message: `Bucket "${name}" was deleted successfully`
							})
						})
						.catch(function (err) {
							reject({
								status: 400,
								message: err.message
							})
						});
				}).catch(function (err) {
					reject({
						status: 400,
						message: err.message
					})
				});

			})

		}

		self.uploadFile = (bucketName, filePath, shouldSimulateBigFileUploadError = false) => {
			return new Promise((resolve, reject) => {

				if (!bucketName || !filePath) {
					reject({
						status: 400,
						message: 'Invalid parameters'
					})
				}

				getBucketByName(bucketName).then((response) => {
					if (response.status !== 200) {
						reject({
							status: 400,
							message: response.message
						})
					}

					let bucket = response.data
					getUploadUrl(bucket.bucketId).then((response) => {
						if (response.status !== 200) {
							reject({
								status: 400,
								message: response.message
							})
						}

						let uploadUrl = response.data.uploadUrl
						let uploadAuthorizationToken = response.data.authorizationToken

						let fileName = path.basename(filePath)
						let fileSize = fs.statSync(filePath).size
						if (fileSize <= self.credentials.recommendedPartSize) {
							let source = fs.readFileSync(filePath)
							let sha1 = crypto.createHash('sha1').update(source).digest("hex");

							uploadSmallFile(source, fileName, filePath, fileSize, sha1, uploadUrl, uploadAuthorizationToken, bucketName).then((response) => {
								resolve(response);
							}).catch((err) => {
								reject(err);
							})
						} else {
							let eachPartSize = Math.floor(self.credentials.recommendedPartSize / 10);
							let source = fs.createReadStream(filePath, { highWaterMark: eachPartSize })
							uploadBigFile(source, fileName, filePath, fileSize, eachPartSize, bucket.bucketId, bucketName, shouldSimulateBigFileUploadError).then((response) => {
								resolve(response);
							}).catch((err) => {
								reject(err);
							})
						}

					}).catch(function (err) {
						reject({
							status: 400,
							message: err.message
						})
					});

				}).catch(function (err) {
					reject({
						status: 400,
						message: err.message
					})
				});

			})
		}

		self.downloadFile = (bucketName, filename, saveToPath) => {
			if (!bucketName || !filename) {
				return new Promise((resolve, reject) => {
					reject({
						status: 400,
						message: 'Invalid parameters'
					})
				})
			}

			return new Promise((resolve, reject) => {
				axios.get(
					`${self.credentials.downloadUrl}/file/${bucketName}/${filename}`,
					{ headers: { Authorization: self.credentials.authorizationToken } })
					.then(function (response) {
						let source = new Readable();
						source._read = function noop() { }; // redundant? see update below
						source.push(response.data);
						source.push(null);

						let destination = fs.createWriteStream(saveToPath)

						source.on('end', () => {
							resolve({
								status: 200,
								message: `File "${filename}" was downloaded successfully from bucket "${bucketName}"`
							})
						})
						source.pipe(destination)
					})
					.catch(function (err) {
						reject({
							status: 400,
							message: err.message
						})
					});
			})
		}

		self.createFileFromText = (bucketName, filename, object, shouldSimulateFileCorruption = false) => {
			if (!bucketName || !filename || !object) {
				return new Promise((resolve, reject) => {
					reject({
						status: 400,
						message: 'Invalid parameters'
					})
				})
			}

			return new Promise((resolve, reject) => {
				getBucketByName(bucketName).then((response) => {
					if (response.status !== 200) {
						reject({
							status: 400,
							message: response.message
						})
					}

					let bucket = response.data
					getUploadUrl(bucket.bucketId).then((response) => {
						if (response.status !== 200) {
							reject({
								status: 400,
								message: response.message
							})
						}

						let stringifiedObject = JSON.stringify(object)

						let source = new Readable();
						source._read = function noop() { }; // redundant? see update below
						source.push(stringifiedObject);
						source.push(null);

						let uploadUrl = response.data.uploadUrl
						let uploadAuthorizationToken = response.data.authorizationToken
						let fileSize = Buffer.byteLength(stringifiedObject, 'utf8');
						let hash = crypto.createHash('sha1');

						hash.setEncoding('hex');
						hash.write(stringifiedObject);
						hash.end();

						uploadSmallFile(source, filename, filename, fileSize, hash.read(), uploadUrl, uploadAuthorizationToken, bucketName).then((response) => {
							// Check whether file was properly uploaded
							let localFileCrc32 = crc32.calculate(stringifiedObject);
							self.readFile(bucketName, filename).then((response) => {
								let remoteFileCrc32
								let data = JSON.stringify(response.data)
								if (shouldSimulateFileCorruption) {
									data += 'corrupted-sufix'
								}
								remoteFileCrc32 = crc32.calculate(data);

								if (localFileCrc32 === remoteFileCrc32) {
									resolve({
										status: 200,
										message: `Object "${filename}" was saved successfully in bucket "${bucketName}"`
									})
								} else {
									reject({
										status: 400,
										message: `CRC32 validation error. Object "${filename}" could not be uploaded successfully`
									})
								}
							})
						})
							.catch(function (err) {
								reject({
									status: 400,
									message: err.message
								})
							});
					}).catch(function (err) {
						reject({
							status: 400,
							message: err.message
						})
					});

				}).catch(function (err) {
					reject({
						status: 400,
						message: err.message
					})
				});

			})
		}

		self.getListOfFiles = (bucketName) => {
			if (!bucketName) {
				return new Promise((resolve, reject) => {
					reject({
						status: 400,
						message: 'Invalid parameters'
					})
				})
			}

			return new Promise((resolve, reject) => {
				getBucketByName(bucketName).then((response) => {
					if (response.status !== 200) {
						reject({
							status: 400,
							message: response.message
						})
					}

					let bucket = response.data
					axios.post(
						`${self.credentials.apiUrl}/b2api/v1/b2_list_file_names`,
						{
							bucketId: bucket.bucketId
						},
						{ headers: { Authorization: self.credentials.authorizationToken } })
						.then(function (response) {
							let files = response.data.files
							let returnedFiles = []
							files.forEach((element) => {
								returnedFiles.push(
									{
										filename: element.fileName
									}
								)

							})
							resolve({
								status: 200,
								data: returnedFiles,
								message: `The list of objects was fetched successfully from bucket "${bucketName}"`
							})
						})
						.catch(function (err) {
							reject({
								status: 400,
								message: err.message
							})
						});
				}).catch(function (err) {
					reject({
						status: 400,
						message: err.message
					})
				});
			});
		}

		self.deleteFile = (bucketName, filename) => {
			if (!bucketName || !filename) {
				return new Promise((resolve, reject) => {
					reject({
						status: 400,
						message: 'Invalid parameters'
					})
				})
			}

			return new Promise((resolve, reject) => {
				getFileByName(bucketName, filename).then((response) => {
					if (response.status !== 200) {
						reject({
							status: 400,
							message: response.message
						})
					}
					let file = response.data
					axios.post(
						`${self.credentials.apiUrl}/b2api/v1/b2_delete_file_version`,
						{
							fileName: filename,
							fileId: file.fileId
						},
						{ headers: { Authorization: self.credentials.authorizationToken } })
						.then(function (response) {
							let files = response.data.files
							resolve({
								status: 200,
								message: `Object "${filename}" was deleted successfully from bucket "${bucketName}"`
							})
						})
						.catch(function (err) {
							reject({
								status: 400,
								message: err.message
							})
						});
				}).catch(function (err) {
					reject({
						status: 400,
						message: err.message
					})
				});
			});
		}

		self.readFile = (bucketName, filename) => {
			if (!bucketName || !filename) {
				return new Promise((resolve, reject) => {
					reject({
						status: 400,
						message: 'Invalid parameters'
					})
				})
			}

			return new Promise((resolve, reject) => {
				axios.get(
					`${self.credentials.downloadUrl}/file/${bucketName}/${filename}`,
					{ headers: { Authorization: self.credentials.authorizationToken } })
					.then(function (response) {
						resolve({
							status: 200,
							data: response.data,
							message: `Object "${filename}" was fetched successfully from bucket "${bucketName}"`
						})
					})
					.catch(function (err) {
						reject({
							status: 400,
							message: err.message
						})
					});
			})
		}
	}
}
module.exports = {
	getB2,
	B2Wrapper
}