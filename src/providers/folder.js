const fs = require('fs');
const path = require('path');
const crc32 = require('fast-crc32c');


function getRootFolder(options) {
	let folderPath = options.folderPath
	return new FolderWrapper(folderPath)
}

function FolderWrapper(folderPath) {
	let rootFolder = folderPath
	let self = this

	self.createBucket = function (name) {
		if (!name) {
			return new Promise(function (resolve, reject) {
				reject({
					status: 400,
					message: 'Invalid parameters'
				})
			})
		}

		return new Promise(function (resolve, reject) {
			fs.mkdir(path.join(rootFolder, name), function (err) {
				if (!err) {
					resolve({
						status: 201,
						message: `Bucket "${name}" was created successfully`
					})
				} else {
					reject({
						status: 400,
						message: err.message
					})
				}
			});
		});
	}

	self.deleteBucket = function (name) {
		if (!name) {
			return new Promise(function (resolve, reject) {
				reject({
					status: 400,
					message: 'Invalid parameters'
				})
			})
		}

		return new Promise(function (resolve, reject) {
			fs.rmdir(path.join(rootFolder, name), function (err) {
				if (!err) {
					resolve({
						status: 200,
						message: `Bucket "${name}" was deleted successfully`
					})
				} else {
					reject({
						status: 400,
						message: err.message
					})
				}
			});
		});
	}

	self.uploadFile = (bucketName, filePath) => {
		if (!bucketName || !filePath) {
			return new Promise((resolve, reject) => {
				reject({
					status: 400,
					message: 'Invalid parameters'
				})
			})
		}

		return new Promise((resolve, reject) => {
			let filename = path.basename(filePath)
			let source = fs.createReadStream(filePath)
			let dest = fs.createWriteStream(path.join(rootFolder, bucketName, filename))

			source.on('error', (err) => {
				reject({
					status: 400,
					message: err.message
				})
			})

			source.on('end', () => {
				resolve({
					status: 200,
					message: `File "${filePath}" was uploaded successfully to bucket "${bucketName}"`
				})
			})

			source.pipe(dest);
		});
	}

	self.downloadFile = (bucketName, filename, saveToPath) => {
		if (!bucketName || !filename || !saveToPath) {
			return new Promise((resolve, reject) => {
				reject({
					status: 400,
					message: 'Invalid parameters'
				})
			})
		}
		return new Promise((resolve, reject) => {
			let source = fs.createReadStream(path.join(rootFolder, bucketName, filename))
			let dest = fs.createWriteStream(saveToPath)

			source.on('error', (err) => {
				reject({
					status: 400,
					message: err.message
				})
			})

			source.on('end', () => {
				resolve({
					status: 200,
					message: `File "${filename}" was downloaded successfully from bucket "${bucketName}"`
				})
			})

			source.pipe(dest);
		});
	}


	self.createFileFromText = function (bucketName, filename, object, shouldSimulateFileCorruption = false) {
		if (!bucketName || !filename || !object) {
			return new Promise(function (resolve, reject) {
				reject({
					status: 400,
					message: 'Invalid parameters'
				})
			})
		}

		return new Promise(function (resolve, reject) {
			let stringifiedObject = JSON.stringify(object)
			fs.writeFile(path.join(rootFolder, bucketName, filename), stringifiedObject, function (err) {
				if (!err) {
					// Check whether file was properly uploaded
					let localFileCrc32 = crc32.calculate(stringifiedObject);
					self.readFile(bucketName, filename).then((response) => {
						let remoteFileCrc32
						let data = JSON.stringify(response.data)
						if (shouldSimulateFileCorruption) {
							data += 'corrupted-sufix'
						}
						remoteFileCrc32 = crc32.calculate(data);

						if (localFileCrc32 === remoteFileCrc32) {
							resolve({
								status: 200,
								message: `Object "${filename}" was saved successfully in bucket "${bucketName}"`
							})
						} else {
							reject({
								status: 400,
								message: `CRC32 validation error. Object "${filename}" could not be uploaded successfully`
							})
						}
					})
				} else {
					reject({
						status: 400,
						message: err.message
					})
				}
			});
		});
	}

	self.getListOfFiles = function (bucketName) {
		if (!bucketName) {
			return new Promise(function (resolve, reject) {
				reject({
					status: 400,
					message: 'Invalid parameters'
				})
			})
		}

		return new Promise(function (resolve, reject) {
			fs.readdir(path.join(rootFolder, bucketName), function (err, data) {
				if (!err) {
					let objects = []
					data.forEach(element => {
						objects.push({
							filename: element
						})
					});
					resolve({
						status: 200,
						data: objects,
						message: `The list of objects was fetched successfully from bucket "${bucketName}"`
					})
				} else {
					reject({
						status: 400,
						message: err.message
					})
				}
			});
		});
	}

	self.deleteFile = function (bucketName, filename) {
		if (!bucketName || !filename) {
			return new Promise(function (resolve, reject) {
				reject({
					status: 400,
					message: 'Invalid parameters'
				})
			})
		}

		return new Promise(function (resolve, reject) {
			fs.unlink(path.join(rootFolder, bucketName, filename), function (err) {
				if (!err) {
					resolve({
						status: 200,
						message: `Object "${filename}" was deleted successfully from bucket "${bucketName}"`
					})
				} else {
					reject({
						status: 400,
						message: err.message
					})
				}
			});
		});
	}

	self.readFile = function (bucketName, filename) {
		if (!bucketName || !filename) {
			return new Promise(function (resolve, reject) {
				reject({
					status: 400,
					message: 'Invalid parameters'
				})
			})
		}

		return new Promise(function (resolve, reject) {
			fs.readFile(path.join(rootFolder, bucketName, filename), 'utf8', function (err, object) {
				if (!err) {
					resolve({
						status: 200,
						data: JSON.parse(object),
						message: `Object "${filename}" was fetched successfully from bucket "${bucketName}"`
					})
				} else {
					reject({
						status: 400,
						message: err.message
					})
				}
			});
		})
	}
}
module.exports = {
	getRootFolder
}