const fs = require('fs');
const pkgcloud = require('pkgcloud');
const Readable = require('stream').Readable
const crc32 = require('fast-crc32c');
const path = require('path');

function getRACKSPACE(options) {
	let rackspace = pkgcloud.storage.createClient({
		provider: 'rackspace',
		username: options.username,
		apiKey: options.apiKey,
		region: options.region || 'IAD'
	});
	return new RackspaceWrapper(rackspace)
}

class RackspaceWrapper {
	constructor(rackspace) {
		this.rackspace = rackspace
		let self = this

		self.createBucket = (name) => {
			if (!name) {
				return new Promise((resolve, reject) => {
					reject({
						status: 400,
						message: 'Invalid parameters'
					})
				})
			}

			return new Promise((resolve, reject) => {
				self.rackspace.createContainer({
					name: name
				}, function (err, data) {
					if (!err) {
						resolve({
							status: 201,
							message: `Bucket "${name}" was created successfully`
						})
					} else {
						reject({
							status: 400,
							message: err.message
						})
					}
				});
			});
		}

		self.deleteBucket = (name) => {
			if (!name) {
				return new Promise((resolve, reject) => {
					reject({
						status: 400,
						message: 'Invalid parameters'
					})
				})
			}

			return new Promise((resolve, reject) => {
				self.rackspace.destroyContainer(name, (err) => {
					if (!err) {
						resolve({
							status: 200,
							message: `Bucket "${name}" was deleted successfully`
						})
					} else {
						reject({
							status: 400,
							message: err.message
						})
					}
				});
			});
		}

		self.uploadFile = (bucketName, filePath) => {
			if (!bucketName || !filePath) {
				return new Promise((resolve, reject) => {
					reject({
						status: 400,
						message: 'Invalid parameters'
					})
				})
			}

			let filename = path.basename(filePath)
			let params = {
				container: bucketName,
				remote: filename
			}

			return new Promise((resolve, reject) => {
				let source = fs.createReadStream(filePath);
				let dest = self.rackspace.upload(params)

				dest.on('error', (err) => {
					reject({
						status: 400,
						message: err.message
					})
				})

				dest.on('end', () => {
					resolve({
						status: 200,
						message: `File "${filePath}" was uploaded successfully to bucket "${bucketName}"`
					})
				})

				source.pipe(dest)
			});
		}

		self.downloadFile = (bucketName, filename, saveToPath) => {
			if (!bucketName || !filename || !saveToPath) {
				return new Promise((resolve, reject) => {
					reject({
						status: 400,
						message: 'Invalid parameters'
					})
				})
			}
			let params = {
				container: bucketName,
				remote: filename
			}
			let dest = fs.createWriteStream(saveToPath);

			return new Promise((resolve, reject) => {
				let source = self.rackspace.download(params)

				source.on('error', (err) => {
					reject({
						status: 400,
						message: err.message
					})
				})

				source.on('end', () => {
					resolve({
						status: 200,
						message: `File "${filename}" was downloaded successfully from bucket "${bucketName}"`
					})
				})

				source.pipe(dest)
			});
		}

		self.createFileFromText = (bucketName, filename, object, shouldSimulateFileCorruption = false) => {
			if (!bucketName || !filename || !object) {
				return new Promise((resolve, reject) => {
					reject({
						status: 400,
						message: 'Invalid parameters'
					})
				})
			}

			return new Promise((resolve, reject) => {
				let stream = new Readable();
				let stringifiedObject = JSON.stringify(object)
				stream.push(stringifiedObject)
				stream.push(null)

				// create a writeable stream for our destination
				let dest = self.rackspace.upload({
					container: bucketName,
					remote: filename
				})

				dest.on('error', (err) => {
					reject({
						status: 400,
						message: err.message
					})
				})

				dest.on('success', (file) => {
					// Check whether file was properly uploaded
					let localFileCrc32 = crc32.calculate(stringifiedObject);
					self.readFile(bucketName, filename).then((response) => {
						let remoteFileCrc32
						let data = JSON.stringify(response.data)
						if (shouldSimulateFileCorruption) {
							data += 'corrupted-sufix'
						}
						remoteFileCrc32 = crc32.calculate(data);

						if (localFileCrc32 === remoteFileCrc32) {
							resolve({
								status: 200,
								message: `Object "${filename}" was saved successfully in bucket "${bucketName}"`
							})
						} else {
							reject({
								status: 400,
								message: `CRC32 validation error. Object "${filename}" could not be uploaded successfully`
							})
						}
					})
				});

				// pipe the source to the destination
				stream.pipe(dest);
			});
		}

		self.getListOfFiles = (bucketName) => {
			if (!bucketName) {
				return new Promise((resolve, reject) => {
					reject({
						status: 400,
						message: 'Invalid parameters'
					})
				})
			}

			return new Promise((resolve, reject) => {
				self.rackspace.getFiles(bucketName, (err, data) => {
					if (!err) {
						let objects = []
						data.forEach(element => {
							objects.push({
								filename: element.name
							})
						});
						resolve({
							status: 200,
							data: objects,
							message: `The list of objects was fetched successfully from bucket "${bucketName}"`
						})
					} else {
						reject({
							status: 400,
							message: err.message
						})
					}
				});
			});
		}

		self.deleteFile = (bucketName, filename) => {
			if (!bucketName || !filename) {
				return new Promise((resolve, reject) => {
					reject({
						status: 400,
						message: 'Invalid parameters'
					})
				})
			}

			return new Promise((resolve, reject) => {
				self.rackspace.removeFile(bucketName, filename, (err, data) => {
					if (!err) {
						resolve({
							status: 200,
							message: `Object "${filename}" was deleted successfully from bucket "${bucketName}"`
						})
					} else {
						reject({
							status: 400,
							message: err.message
						})
					}
				});
			});
		}

		self.readFile = (bucketName, filename) => {
			if (!bucketName || !filename) {
				return new Promise((resolve, reject) => {
					reject({
						status: 400,
						message: 'Invalid parameters'
					})
				})
			}

			let objectData
			return new Promise((resolve, reject) => {
				let source = self.rackspace.download({
					container: bucketName,
					remote: filename
				}, function (err, data) {
					if (err) {
						reject({
							status: 400,
							message: err.message
						})
					}
				});

				source.on('data', (data) => {
					objectData = data
				})
				source.on('end', () => {
					resolve({
						status: 200,
						data: JSON.parse(objectData.toString('utf8')),
						message: `Object "${filename}" was fetched successfully from bucket "${bucketName}"`
					})
				})
			})
		}
	}
}
module.exports = {
	getRACKSPACE
}