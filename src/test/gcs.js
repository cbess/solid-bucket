const assert = require('assert');

const fs = require('fs');
const slashes = require('slashes');

const SolidBucket = require('../index')
const {resolveUniversalApiWorkflow, simulateObjectCorruptionWorkflow, rejectMissingParametersWorkflow, rejectUniversalApiWorkflow} = require('./utils')


let applyCredentials = (task) => {
	let options = {
		"type": "service_account",
		"project_id": process.env.GCS_PROJECTID,
		"private_key_id": process.env.GCS_PRIVATEKEYID,
		"private_key": slashes.strip(process.env.GCS_PRIVATEKEY),
		"client_email": process.env.GCS_CLIENTEMAIL,
		"client_id": process.env.GCS_CLIENTID,
		"auth_uri": "https://accounts.google.com/o/oauth2/auth",
		"token_uri": "https://accounts.google.com/o/oauth2/token",
		"auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
		"client_x509_cert_url": process.env.GCS_CLIENTX509CERTURL
	}

	let optionsJson = JSON.stringify(options);
	fs.writeFile('gcs-credentials.json', optionsJson, 'utf8', () => {
		let keyFilename = 'gcs-credentials.json'

		task(keyFilename)
	});

}

describe('Google Cloud Storage', function () {
	this.timeout(15000);

	describe('Universal API', function () {
		it('should test the happy path', function () {
			function task(keyFilename) {
				let bucketName = String(Math.floor(Math.random() * 200000000) * Math.floor(Math.random() * 200000000)) + 'tests';
				let filePath = 'lib/test/testdata/binary.bin'
				let filenameTxt = 'generated-txt.txt'

				let provider = new SolidBucket('gcs', {
					keyFilename: keyFilename
				})

				resolveUniversalApiWorkflow(provider, bucketName, filePath, filenameTxt)

				fs.unlinkSync(keyFilename);
			}

			applyCredentials(task)
		});

		it('should test against missing parameters', function () {
			function task(keyFilename) {
				let bucketName = '';
				let filename = ''

				let provider = new SolidBucket('gcs', {
					keyFilename: keyFilename
				})

				rejectMissingParametersWorkflow(provider, bucketName, filename)

				fs.unlinkSync(keyFilename);
			}

			applyCredentials(task)
		});

		it('should test against missing auth options', function () {
			assert.throws(() => {
				new SolidBucket('gcs', {
					keyFilename: ''
				})
			}, Error)
		});

		it('should test object upload corruptions', function () {
			function task(keyFilename) {
				let bucketName = String(Math.floor(Math.random() * 200000000) * Math.floor(Math.random() * 200000000)) + 'tests';
				let filename = 'generated-during-tests.txt'

				let provider = new SolidBucket('gcs', {
					keyFilename: keyFilename
				})

				simulateObjectCorruptionWorkflow(provider, bucketName, filename)

				fs.unlinkSync(keyFilename);
			}

			applyCredentials(task)
		});

		it('should test the provider rejections', function () {
			function task(keyFilename) {

				let bucketName = 'backup'
				let filename = 'generated-during-tests.txt'

				let provider = new SolidBucket('gcs', {
					keyFilename: keyFilename
				})

				rejectUniversalApiWorkflow(provider, bucketName, filename)

				fs.unlinkSync(keyFilename);
			}

			applyCredentials(task)
		});
	});
});