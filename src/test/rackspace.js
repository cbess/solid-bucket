const assert = require('assert');

const SolidBucket = require('../index')
const {resolveUniversalApiWorkflow, simulateObjectCorruptionWorkflow, rejectMissingParametersWorkflow, rejectUniversalApiWorkflow} = require('./utils')

let username = process.env.RACKSPACE_USERNAME
let apiKey = process.env.RACKSPACE_APIKEY

describe('Rackspace Cloud Storage', function () {
	this.timeout(15000);

	describe('Universal API', function () {
		it('should test the happy path', function () {
			let bucketName = String(Math.floor(Math.random() * 200000000) * Math.floor(Math.random() * 200000000)) + 'tests';
			let filePath = 'lib/test/testdata/binary.bin'
			let filenameTxt = 'generated-txt.txt'

			let provider = new SolidBucket('rackspace', {
				username: username,
				apiKey: apiKey
			})

			resolveUniversalApiWorkflow(provider, bucketName, filePath, filenameTxt)
		});

		it('should test against missing parameters', function () {
			let bucketName = '';
			let filename = ''

			let provider = new SolidBucket('rackspace', {
				username: username,
				apiKey: apiKey				
			})

			rejectMissingParametersWorkflow(provider, bucketName, filename)
		});

		it('should test against missing auth options', function () {
			assert.throws(() => {new SolidBucket('rackspace', {
				username: '',
				apiKey: ''
			})}, Error)
		});

		it('should test object upload corruptions', function () {
			let bucketName = String(Math.floor(Math.random() * 200000000) * Math.floor(Math.random() * 200000000)) + 'tests';
			let filename = 'generated-during-tests.txt'

			let provider = new SolidBucket('rackspace', {
				username: username,
				apiKey: apiKey
			})

			simulateObjectCorruptionWorkflow(provider, bucketName, filename)
		});

		it('should test the provider rejections', function () {
			let bucketName = 'backup';
			let filename = 'generated-during-tests.txt'

			let provider = new SolidBucket('rackspace', {
				username: username,
				apiKey: apiKey
			})
			rejectUniversalApiWorkflow(provider, bucketName, filename)
		});
	});
});