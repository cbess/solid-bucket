const assert = require('assert');

const SolidBucket = require('../index')
const {resolveUniversalApiWorkflow, simulateObjectCorruptionWorkflow, rejectMissingParametersWorkflow, rejectUniversalApiWorkflow} = require('./utils')

let endpoint = process.env.S3COMPATIBLE_ENDPOINT
let accessKeyId = process.env.S3COMPATIBLE_ACCESSKEYID
let secretAccessKey = process.env.S3COMPATIBLE_SECRETACCESSKEY

describe('S3 Compatible', function () {
	this.timeout(15000);

	describe('Universal API', function () {
		it('should test the happy path', function () {
			let bucketName = String(Math.floor(Math.random() * 200000000) * Math.floor(Math.random() * 200000000)) + 'tests';
			let filePath = 'lib/test/testdata/binary.bin'
			let filenameTxt = 'generated-txt.txt'

			let provider = new SolidBucket('s3compatible', {
				endpoint: endpoint,
				accessKeyId: accessKeyId,
				secretAccessKey: secretAccessKey
			})

			resolveUniversalApiWorkflow(provider, bucketName, filePath, filenameTxt)
		});

		it('should test against missing parameters', function () {
			let bucketName = '';
			let filename = ''

			let provider = new SolidBucket('s3compatible', {
				endpoint: endpoint,
				accessKeyId: accessKeyId,
				secretAccessKey: secretAccessKey
			})
			rejectMissingParametersWorkflow(provider, bucketName, filename)
		});

		it('should test against missing auth options', function () {
			assert.throws(() => {new SolidBucket('s3compatible', {
				endpoint: '',
				accessKeyId: '',
				secretAccessKey: ''
			})}, Error)
		});

		it('should test object upload corruptions', function () {
			let bucketName = String(Math.floor(Math.random() * 200000000) * Math.floor(Math.random() * 200000000)) + 'tests';
			let filename = 'generated-during-tests.txt'

			let provider = new SolidBucket('s3compatible', {
				endpoint: endpoint,
				accessKeyId: accessKeyId,
				secretAccessKey: secretAccessKey
			})

			simulateObjectCorruptionWorkflow(provider, bucketName, filename)
		});

		it('should test the provider rejections', function () {
			let bucketName = 'backup';
			let filename = 'generated-during-tests.txt'

			let provider = new SolidBucket('s3compatible', {
				endpoint: endpoint,
				accessKeyId: accessKeyId,
				secretAccessKey: secretAccessKey
			})
			rejectUniversalApiWorkflow(provider, bucketName, filename)
		});
	});
});