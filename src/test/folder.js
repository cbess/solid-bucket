const assert = require('assert');

const SolidBucket = require('../index')
const {resolveUniversalApiWorkflow, simulateObjectCorruptionWorkflow, rejectMissingParametersWorkflow, rejectUniversalApiWorkflow} = require('./utils')

let folderPath = process.env.FOLDER_FOLDERPATH;

describe('Folder', function () {
	this.timeout(15000);

	describe('Universal API', function () {
		it('should test the happy path', function () {
			let bucketName = String(Math.floor(Math.random() * 200000000) * Math.floor(Math.random() * 200000000)) + 'tests';
			let filePath = 'lib/test/testdata/binary.bin'
			let filenameTxt = 'generated-txt.txt'

			let provider = new SolidBucket('folder', {
				folderPath: folderPath
			})

			resolveUniversalApiWorkflow(provider, bucketName, filePath, filenameTxt)
		});

		it('should test against missing parameters', function () {
			let bucketName = '';
			let filename = ''

			let provider = new SolidBucket('folder', {
				folderPath: folderPath
			})
			rejectMissingParametersWorkflow(provider, bucketName, filename)
		});

		it('should test against missing auth options', function () {
			assert.throws(() => {new SolidBucket('folder', {
				folderPath: ''
			})}, Error)
		});

		it('should test object upload corruptions', function () {
			let bucketName = String(Math.floor(Math.random() * 200000000) * Math.floor(Math.random() * 200000000)) + 'tests';
			let filename = 'generated-during-tests.txt'

			let provider = new SolidBucket('folder', {
				folderPath: folderPath
			})

			simulateObjectCorruptionWorkflow(provider, bucketName, filename)
		});

		it('should test the provider rejections', function () {
			// Empty bucketName
			let bucketName = ''
			let filename = 'generated-during-tests.txt'

			let provider = new SolidBucket('folder', {
				folderPath: folderPath
			})

			rejectUniversalApiWorkflow(provider, bucketName, filename)
		});
	});
});