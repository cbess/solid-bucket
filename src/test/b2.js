const assert = require('assert');

const SolidBucket = require('../index')
const {
	resolveUniversalApiWorkflow,
	simulateObjectCorruptionWorkflow,
	uploadBigFileWorkflow,
	simulateBigFileUploadErrorWorkflow,
	rejectMissingParametersWorkflow,
	rejectUniversalApiWorkflow
} = require('./utils')

let accountId = process.env.B2_ACCOUNTID
let applicationKey = process.env.B2_APPLICATIONKEY

describe('Backblaze B2', function () {
	this.timeout(15000);

	describe('Universal API', function () {
		it('should test the happy path', function () {
			let bucketName = String(Math.floor(Math.random() * 200000000) * Math.floor(Math.random() * 200000000)) + 'tests';
			let filePath = 'lib/test/testdata/binary.bin'
			let filenameTxt = 'generated-txt.txt'

			new SolidBucket('b2', {
				accountId: accountId,
				applicationKey: applicationKey
			}).then((provider) => {
				resolveUniversalApiWorkflow(provider, bucketName, filePath, filenameTxt)
			}).catch((err) => {
				console.log(err)
			})
		});

		it('should test against missing parameters', function () {
			let bucketName = '';
			let filename = ''

			new SolidBucket('b2', {
				accountId: accountId,
				applicationKey: applicationKey
			}).then((provider) => {
				rejectMissingParametersWorkflow(provider, bucketName, filename)
			}).catch((err) => {
				console.log(err)
			})
		});

		it('should test against missing auth options', function () {
			assert.throws(() => {new SolidBucket('b2', {
				accountId: '',
				applicationKey: ''
			})}, Error)
		});

		it('should test object upload corruptions', function () {
			let bucketName = String(Math.floor(Math.random() * 200000000) * Math.floor(Math.random() * 200000000)) + 'tests';
			let filename = 'generated-during-tests.txt'

			new SolidBucket('b2', {
				accountId: accountId,
				applicationKey: applicationKey
			}).then((provider) => {
				simulateObjectCorruptionWorkflow(provider, bucketName, filename)
			}).catch((err) => {
				console.log(err)
			})
		});

		it('should test big file upload', function () {
			let bucketName = String(Math.floor(Math.random() * 200000000) * Math.floor(Math.random() * 200000000)) + 'tests';
			let filePath = 'lib/test/testdata/bigbinary.bin'

			new SolidBucket('b2', {
				accountId: accountId,
				applicationKey: applicationKey
			}).then((provider) => {
				uploadBigFileWorkflow(provider, bucketName, filePath)
			}).catch((err) => {
				console.log(err)
			})
		});

		it('should test big file upload errors', function () {
			let bucketName = String(Math.floor(Math.random() * 200000000) * Math.floor(Math.random() * 200000000)) + 'tests';
			let filePath = 'lib/test/testdata/bigbinary.bin'

			new SolidBucket('b2', {
				accountId: accountId,
				applicationKey: applicationKey
			}).then((provider) => {
				simulateBigFileUploadErrorWorkflow(provider, bucketName, filePath)
			}).catch((err) => {
				console.log(err)
			})
		});

		it('should test the provider rejections', function () {
			let bucketName = 'backup';
			let filename = 'generated-during-tests.txt'

			let provider = new SolidBucket('b2', {
				accountId: accountId,
				applicationKey: applicationKey
			}).then((provider) => {
				rejectUniversalApiWorkflow(provider, bucketName, filename)
			}).catch((err) => {
				console.log(err)
			})
		});
	});
});