const assert = require('assert');
const path = require('path');
const fs = require('fs');


let resolveUniversalApiWorkflow = (provider, bucketName, filePath, filenameTxt) => {

    provider.createBucket(bucketName).then((data) => {
        console.log(data)
        assert.deepEqual(data, {
            status: 201,
            message: `Bucket "${bucketName}" was created successfully`
        })

        provider.uploadFile(bucketName, filePath).then((data) => {
            console.log(data)
            assert.deepEqual(data, {
                status: 200,
                message: `File "${filePath}" was uploaded successfully to bucket "${bucketName}"`
            })

            let filename = path.basename(filePath)
            provider.downloadFile(bucketName, filename, 'downloaded-binary.bin').then((data) => {
                console.log(data)
                assert.deepEqual(data, {
                    status: 200,
                    message: `File "${filename}" was downloaded successfully from bucket "${bucketName}"`
                })

                let existsFile = fs.existsSync('downloaded-binary.bin')
                assert.equal(existsFile, true)

                // Delete downloaded file
                fs.unlinkSync('downloaded-binary.bin')

                let txt = 'This is a random text \n Second Line'
                provider.createFileFromText(bucketName, filenameTxt, txt).then((data) => {
                    console.log(data)
                    assert.deepEqual(data, {
                        status: 200,
                        message: `Object "${filenameTxt}" was saved successfully in bucket "${bucketName}"`
                    })

                    provider.readFile(bucketName, filenameTxt).then((data) => {
                        console.log(data)
                        assert.deepEqual(data, {
                            status: 200,
                            data: txt,
                            message: `Object "${filenameTxt}" was fetched successfully from bucket "${bucketName}"`
                        })

                        provider.getListOfFiles(bucketName).then((data) => {
                            console.log(data)
                            assert.deepEqual(data, {
                                status: 200,
                                data: [
                                    {
                                        filename: filename
                                    },
                                    {
                                        filename: filenameTxt
                                    }
                                ],
                                message: `The list of objects was fetched successfully from bucket "${bucketName}"`
                            })

                            provider.deleteFile(bucketName, filename).then((data) => {
                                console.log(data)
                                assert.deepEqual(data, {
                                    status: 200,
                                    message: `Object "${filename}" was deleted successfully from bucket "${bucketName}"`
                                })

                                provider.deleteFile(bucketName, filenameTxt).then((data) => {
                                    console.log(data)
                                    assert.deepEqual(data, {
                                        status: 200,
                                        message: `Object "${filenameTxt}" was deleted successfully from bucket "${bucketName}"`
                                    })

                                    provider.deleteBucket(bucketName).then((data) => {
                                        console.log(data)
                                        assert.deepEqual(data, {
                                            status: 200,
                                            message: `Bucket "${bucketName}" was deleted successfully`
                                        })
                                    }).catch((data) => console.log(data))
                                }).catch((data) => console.log(data))
                            }).catch((data) => console.log(data))
                        }).catch((data) => console.log(data))
                    }).catch((data) => console.log(data))
                }).catch((data) => console.log(data))
            }).catch((data) => console.log(data))
        }).catch((data) => console.log(data))
    }).catch((data) => console.log(data))
}

let simulateObjectCorruptionWorkflow = (provider, bucketName, filename) => {

    provider.createBucket(bucketName).then((data) => {
        console.log(data)
        assert.deepEqual(data, {
            status: 201,
            message: `Bucket "${bucketName}" was created successfully`
        })

        let txt = 'This is a random text \n Second Line'

        let shouldSimulateFileCorruption = true
        provider.createFileFromText(bucketName, filename, txt, shouldSimulateFileCorruption).catch((data) => {
            console.log(data)
            assert.deepEqual(data, {
                status: 400,
                message: `CRC32 validation error. Object "${filename}" could not be uploaded successfully`
            })

            provider.deleteFile(bucketName, filename).then((data) => {
                console.log(data)
                assert.deepEqual(data, {
                    status: 200,
                    message: `Object "${filename}" was deleted successfully from bucket "${bucketName}"`
                })

                provider.deleteBucket(bucketName).then((data) => {
                    console.log(data)
                    assert.deepEqual(data, {
                        status: 200,
                        message: `Bucket "${bucketName}" was deleted successfully`
                    })
                }).catch((data) => console.log(data))
            }).catch((data) => console.log(data))
        })
    }).catch((data) => console.log(data))
}

let simulateBigFileUploadErrorWorkflow = (provider, bucketName, filePath) => {
    provider.createBucket(bucketName).then((data) => {
        console.log(data)
        assert.deepEqual(data, {
            status: 201,
            message: `Bucket "${bucketName}" was created successfully`
        })

        let shouldSimulateBigFileUploadError = true
        provider.uploadFile(bucketName, filePath, shouldSimulateBigFileUploadError).catch((data) => {
            console.log(data)
            assert.deepEqual(data, {
                status: 400,
                message: `Large File "${filePath}" upload was unsuccessful`
            })

                provider.deleteBucket(bucketName).then((data) => {
                    console.log(data)
                    assert.deepEqual(data, {
                        status: 200,
                        message: `Bucket "${bucketName}" was deleted successfully`
                    })
            }).catch((data) => console.log(data))
        })
    }).catch((data) => console.log(data))
}

let uploadBigFileWorkflow = (provider, bucketName, filePath) => {
    provider.createBucket(bucketName).then((data) => {
        console.log(data)
        assert.deepEqual(data, {
            status: 201,
            message: `Bucket "${bucketName}" was created successfully`
        })

        provider.uploadFile(bucketName, filePath).then((data) => {
            console.log(data)
            assert.deepEqual(data, {
                status: 200,
                message: `File "${filePath}" was uploaded successfully to bucket "${bucketName}"`
            })

            let filename = path.basename(filePath)
            provider.deleteFile(bucketName, filename).then((data) => {
                console.log(data)
                assert.deepEqual(data, {
                    status: 200,
                    message: `Object "${filename}" was deleted successfully from bucket "${bucketName}"`
                })

                provider.deleteBucket(bucketName).then((data) => {
                    console.log(data)
                    assert.deepEqual(data, {
                        status: 200,
                        message: `Bucket "${bucketName}" was deleted successfully`
                    })
                }).catch((data) => console.log(data))
            }).catch((data) => console.log(data))
        }).catch((data) => console.log(data))
    }).catch((data) => console.log(data))

}


let rejectMissingParametersWorkflow = (provider, bucketName, filename) => {
    provider.createBucket(bucketName).catch((data) => {
        assert.deepEqual(data, {
            status: 400,
            message: 'Invalid parameters'
        })
        provider.createFileFromText(bucketName, filename, {}).catch((data) => {
            assert.deepEqual(data, {
                status: 400,
                message: 'Invalid parameters'
            })
            provider.readFile(bucketName, filename).catch((data) => {
                assert.deepEqual(data, {
                    status: 400,
                    message: 'Invalid parameters'
                })
                provider.getListOfFiles(bucketName).catch((data) => {
                    assert.deepEqual(data, {
                        status: 400,
                        message: 'Invalid parameters'
                    })
                    provider.deleteFile(bucketName, filename).catch((data) => {
                        assert.deepEqual(data, {
                            status: 400,
                            message: 'Invalid parameters'
                        })
                        provider.deleteBucket(bucketName).catch((data) => {
                            assert.deepEqual(data, {
                                status: 400,
                                message: 'Invalid parameters'
                            })
                        })
                    })
                })
            })
        })
    })
}

let rejectUniversalApiWorkflow = (provider, bucketName, filename) => {

    provider.createBucket(bucketName).catch((data) => {
        assert.equal(data.status, 400)

        provider.createFileFromText(bucketName, filename, { attr1: 2 }).catch((data) => {
            assert.equal(data.status, 400)

            provider.readFile(bucketName, filename).catch((data) => {
                assert.equal(data.status, 400)

                provider.getListOfFiles(bucketName).catch((data) => {
                    assert.equal(data.status, 400)

                    provider.deleteFile(bucketName, filename).catch((data) => {
                        assert.equal(data.status, 400)

                        provider.deleteBucket(bucketName).catch((data) => {
                            assert.equal(data.status, 400)

                        })
                    })
                })
            })
        })
    })
}

module.exports = { 
    resolveUniversalApiWorkflow,
    simulateObjectCorruptionWorkflow,
    uploadBigFileWorkflow,
    simulateBigFileUploadErrorWorkflow,
    rejectMissingParametersWorkflow,
    rejectUniversalApiWorkflow
}